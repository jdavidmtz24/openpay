package com.juandavid.openpay.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "activity_user_table")
data class ActivitiesUserEntity (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo("id") var id: Long = 0,
    @ColumnInfo("user") var user: String? = null,
    @ColumnInfo("activity") var activity: String? = null,
        )