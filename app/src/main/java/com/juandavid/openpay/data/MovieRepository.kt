package com.juandavid.openpay.data

import com.juandavid.openpay.data.database.dao.MovieDao
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.data.database.entities.MoviesNowPlayingEntity
import com.juandavid.openpay.data.database.entities.MoviesPopularEntity
import com.juandavid.openpay.data.database.entities.MoviesTopRateEntity
import com.juandavid.openpay.data.model.Results
import com.juandavid.openpay.data.network.MovieService
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val service: MovieService,
    private val movieDao: MovieDao,
) {

    suspend fun getMoviesPopulars(language:String, page:Long):ArrayList<Results>?{
        val response = service.getPopularMovie(language, page)
        //todo realizar metodo de insersion a BD y consulta a BD
        return response!!.results
    }

    suspend fun getMoviesTopRated(language:String, page:Long):ArrayList<Results>?{
        val response = service.getTopRatedMovie(language, page)
        //todo realizar metodo de insersion a BD y consulta a BD
        return response!!.results
    }

    suspend fun getMoviesNowPlaying(language:String, page:Long):ArrayList<Results>?{
        val response = service.getNowPlayingMovie(language, page)
        //todo realizar metodo de insersion a BD y consulta a BD
        return response!!.results
    }

    suspend fun insertMoviesPopular(moviesList:List<MoviesPopularEntity>){
        movieDao.insertMoviePopular(moviesList)
    }
    suspend fun insertMoviesNowPlaying(moviesList:List<MoviesNowPlayingEntity>){
        movieDao.insertMovieNowPlaying(moviesList)
    }
    suspend fun insertMoviesTopRate(moviesList:List<MoviesTopRateEntity>){
        movieDao.insertMovieTopRate(moviesList)
    }

    suspend fun getMoviesPopular(pageMovie: Long): List<MoviesPopularEntity>? {
        return movieDao.getMoviePopularPage(pageMovie = pageMovie)
    }

    suspend fun getMoviesNowPlaying(pageMovie: Long): List<MoviesNowPlayingEntity>? {
        return movieDao.getMovieNowPlayingPage(pageMovie = pageMovie)
    }

    suspend fun getMoviesTopRate(pageMovie: Long): List<MoviesTopRateEntity>? {
        return movieDao.getMovieTopRatePage(pageMovie = pageMovie)
    }

    suspend fun getActivitiesUser(user: String):List<ActivitiesUserEntity>?{
        return movieDao.getActivitiesUser(user = user)
    }

    suspend fun insertActivityUser(activities:List<ActivitiesUserEntity>){
        movieDao.insertActivityUser(activities)
    }
}