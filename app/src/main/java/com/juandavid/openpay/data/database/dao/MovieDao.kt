package com.juandavid.openpay.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.data.database.entities.MoviesNowPlayingEntity
import com.juandavid.openpay.data.database.entities.MoviesPopularEntity
import com.juandavid.openpay.data.database.entities.MoviesTopRateEntity

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMoviePopular(arrayListMovie:List<MoviesPopularEntity>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieNowPlaying(arrayListMovie:List<MoviesNowPlayingEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovieTopRate(arrayListMovie:List<MoviesTopRateEntity>)

    @Query("SELECT * FROM movies_popular_table where page_movie= :pageMovie order by id")
    suspend fun getMoviePopularPage(pageMovie:Long):List<MoviesPopularEntity>

    @Query("SELECT * FROM movies_now_playing_table where page_movie= :pageMovie order by id")
    suspend fun getMovieNowPlayingPage(pageMovie:Long):List<MoviesNowPlayingEntity>

    @Query("SELECT * FROM movies_top_rate_table where page_movie= :pageMovie order by id")
    suspend fun getMovieTopRatePage(pageMovie:Long):List<MoviesTopRateEntity>

    @Query("SELECT * FROM activity_user_table where user= :user order by id desc")
    suspend fun getActivitiesUser(user:String):List<ActivitiesUserEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertActivityUser(arrayListActivity:List<ActivitiesUserEntity>)
}
