package com.juandavid.openpay.data.firebase.model

data class Location(
     val fecha: String,
     val hora: String,
     val latitud: String,
     val longitud: String,
     val marca: String,
     val modelo: String,
     val imei:String
)