package com.juandavid.openpay.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.juandavid.openpay.domain.model.Movie

@Entity(tableName = "movies_popular_table")
data class MoviesPopularEntity(
    @PrimaryKey @ColumnInfo("id") var id: Long? = 0,
    @ColumnInfo("adult") var adult: Boolean? = false,
    @ColumnInfo("backdrop_path") var backdropPath: String? = null,
    @ColumnInfo("genre_ids") var genreIds: String? = null,
    @ColumnInfo("original_language") var originalLanguage: String? = null,
    @ColumnInfo("original_title") var originalTitle: String? = null,
    @ColumnInfo("overview") var overview: String? = null,
    @ColumnInfo("popularity") var popularity: Double? = 0.0,
    @ColumnInfo("poster_path") var posterPath: String? = null,
    @ColumnInfo("release_date") var releaseDate: String? = null,
    @ColumnInfo("title") var title: String? = null,
    @ColumnInfo("video") var video: Boolean? = false,
    @ColumnInfo("vote_average") var voteAverage: Double? = 0.0,
    @ColumnInfo("vote_count") var voteCount: Long? = 0,
    @ColumnInfo("page_movie") var pageMovie: Long? = 0
)

fun Movie.toDatabase() =
    MoviesPopularEntity(adult = adult, backdropPath = backdrop_path, genreIds = genre_ids.toString(), id = id,
        originalLanguage= original_language, originalTitle=original_title, overview=overview,
        popularity = popularity, posterPath=poster_path, releaseDate =release_date, title=title, video = video,
        voteAverage = vote_average, voteCount = vote_count, pageMovie = page_movie)