package com.juandavid.openpay.data.model

import com.google.gson.annotations.SerializedName

data class MoviesPopularResponse (

    @SerializedName("page"          ) var page         : Long?               = 0,
    @SerializedName("results"       ) var results      : ArrayList<Results> = arrayListOf(),
    @SerializedName("total_pages"   ) var totalPages   : Long?               = 0,
    @SerializedName("total_results" ) var totalResults : Long?               = 0

)