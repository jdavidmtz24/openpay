package com.juandavid.openpay.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import com.juandavid.openpay.domain.model.Movie
import com.juandavid.openpay.utils.Constants

data class Results(

    @SerializedName("adult") var adult: Boolean? = false,
    @SerializedName("backdrop_path") var backdropPath: String? = null,
    @SerializedName("genre_ids") var genreIds: ArrayList<Int> = arrayListOf(),
    @SerializedName("id") var id: Long? = 0,
    @SerializedName("original_language") var originalLanguage: String? = null,
    @SerializedName("original_title") var originalTitle: String? = null,
    @SerializedName("overview") var overview: String? = null,
    @SerializedName("popularity") var popularity: Double? = 0.0,
    @SerializedName("poster_path") var posterPath: String? = null,
    @SerializedName("release_date") var releaseDate: String? = null,
    @SerializedName("title") var title: String? = null,
    @SerializedName("video") var video: Boolean? = false,
    @SerializedName("vote_average") var voteAverage: Double? = 0.0,
    @SerializedName("vote_count") var voteCount: Long? = 0,
    @SerializedName("page_movie") var pageMovie: Long? = 0,
    )
fun Movie.toDomain() =
    Results(adult = adult, backdropPath = backdrop_path, genreIds = Constants.converStringtoArray(genre_ids),
        id = id, originalLanguage=original_language, originalTitle=original_title, overview=overview,
        popularity = popularity, posterPath=poster_path, releaseDate =release_date, title=title, video = video,
        voteAverage = vote_average, voteCount = vote_count, pageMovie =page_movie)