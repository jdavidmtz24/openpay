package com.juandavid.openpay.data.network

import com.juandavid.openpay.BuildConfig
import com.juandavid.openpay.data.model.MoviesNowPlayingResponse
import com.juandavid.openpay.data.model.MoviesPopularResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieService @Inject constructor(private val apiClient: ApiClient) {

    suspend fun getPopularMovie(language:String, page:Long):MoviesPopularResponse?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMoviesPopular(BuildConfig.BEARER_TOKEN,language, page)
            if (response.code() == 200) {
                response.body()
            } else {
                null
            }
        }
    }
    suspend fun getTopRatedMovie(language:String, page:Long):MoviesPopularResponse?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMoviesTopRated(BuildConfig.BEARER_TOKEN,language, page)
            if (response.code() == 200) {
                response.body()
            } else {
                null
            }
        }
    }

    suspend fun getNowPlayingMovie(language:String, page:Long):MoviesNowPlayingResponse?{
        return withContext(Dispatchers.IO){
            val response = apiClient.getMoviesNowPlaying(BuildConfig.BEARER_TOKEN, language, page)
            if (response.code() == 200) {
                response.body()
            } else {
                null
            }
        }
    }
}