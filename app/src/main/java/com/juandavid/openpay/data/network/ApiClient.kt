package com.juandavid.openpay.data.network

import com.juandavid.openpay.data.model.MoviesNowPlayingResponse
import com.juandavid.openpay.data.model.MoviesPopularResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiClient {

    @Headers("Content-Type: application/json")
    @GET("popular")
    suspend fun getMoviesPopular(
        @Header("Authorization") token: String,
        @Query("language") language: String,
        @Query("page") page: Long,
    ): Response<MoviesPopularResponse>


    @Headers("Content-Type: application/json")
    @GET("now_playing")
    suspend fun getMoviesNowPlaying(
        @Header("Authorization") token: String,
        @Query("language") language: String,
        @Query("page") page: Long,
    ): Response<MoviesNowPlayingResponse>


    @Headers("Content-Type: application/json")
    @GET("top_rated")
    suspend fun getMoviesTopRated(
        @Header("Authorization") token: String,
        @Query("language") language: String,
        @Query("page") page: Long,
    ): Response<MoviesPopularResponse>
}