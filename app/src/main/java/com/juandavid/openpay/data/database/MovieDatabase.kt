package com.juandavid.openpay.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.juandavid.openpay.data.database.dao.MovieDao
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.data.database.entities.MoviesNowPlayingEntity
import com.juandavid.openpay.data.database.entities.MoviesPopularEntity
import com.juandavid.openpay.data.database.entities.MoviesTopRateEntity

@Database(
    entities = [MoviesPopularEntity::class, MoviesNowPlayingEntity::class, MoviesTopRateEntity::class, ActivitiesUserEntity::class],
    version = 1
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun getMovieDao(): MovieDao
}