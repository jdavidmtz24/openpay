package com.juandavid.openpay.domain

import com.juandavid.openpay.data.MovieRepository
import com.juandavid.openpay.data.database.entities.toDatabase
import com.juandavid.openpay.data.database.entities.toDatabaseNowPlaying
import com.juandavid.openpay.data.model.MoviesNowPlayingResponse
import com.juandavid.openpay.data.model.Results
import com.juandavid.openpay.domain.model.toDomain
import com.juandavid.openpay.utils.Constants
import javax.inject.Inject

class GetMovieNowPlayingUseCase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(language: String, page: Long):ArrayList<Results>? {
        val dbList = repository.getMoviesNowPlaying(pageMovie =  page)
        if (dbList.isNullOrEmpty()){
            val response =repository.getMoviesNowPlaying(language, page)
            if (!response.isNullOrEmpty()) {
                repository.insertMoviesNowPlaying(moviesList = response.map {
                    it.pageMovie = page
                    it.toDomain().toDatabaseNowPlaying()
                })
            }
            return response
        }
        else{
            var arrayMovies:ArrayList<Results> = arrayListOf()
            dbList.forEach {
                var result = Results(adult = it.adult, backdropPath = it.backdropPath,
                    genreIds = Constants.converStringtoArray(it.genreIds!!), id = it.id,
                    originalLanguage = it.originalLanguage, originalTitle = it.originalTitle,
                    overview = it.overview, popularity = it.popularity, posterPath = it.posterPath, releaseDate = it.releaseDate,
                    title = it.title, video = it.video, voteAverage = it.voteAverage, voteCount = it.voteCount,
                    pageMovie = page
                )
                arrayMovies.add(result)
            }
            return arrayMovies
        }
    }
}