package com.juandavid.openpay.domain

import com.juandavid.openpay.data.MovieRepository
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import javax.inject.Inject

class SendActivityUserUseCase @Inject constructor(private val repository: MovieRepository)  {
    suspend operator fun invoke(activities:List<ActivitiesUserEntity>){
        repository.insertActivityUser(activities)
    }
}