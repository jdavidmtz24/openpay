package com.juandavid.openpay.domain

import com.juandavid.openpay.data.MovieRepository
import com.juandavid.openpay.data.database.entities.toDatabaseNowPlaying
import com.juandavid.openpay.data.database.entities.toDatabaseTopRate
import com.juandavid.openpay.data.model.Results
import com.juandavid.openpay.domain.model.toDomain
import com.juandavid.openpay.utils.Constants
import javax.inject.Inject

class GetMovieTopRatedUsecase @Inject constructor(private val repository: MovieRepository) {

    suspend operator fun invoke(language: String, page: Long):ArrayList<Results>? {
        val dbList = repository.getMoviesTopRate(pageMovie =  page)
        if (dbList.isNullOrEmpty()){
            val response =repository.getMoviesTopRated(language, page)
            if (!response.isNullOrEmpty()) {
                repository.insertMoviesTopRate(moviesList = response.map {
                    it.pageMovie = page
                    it.toDomain().toDatabaseTopRate()
                })
            }
            return response
        }
        else{
            var arrayMovies:ArrayList<Results> = arrayListOf()
            dbList.forEach {
                var result = Results(adult = it.adult, backdropPath = it.backdropPath,
                    genreIds = Constants.converStringtoArray(it.genreIds!!), id = it.id,
                    originalLanguage = it.originalLanguage, originalTitle = it.originalTitle,
                    overview = it.overview, popularity = it.popularity, posterPath = it.posterPath, releaseDate = it.releaseDate,
                    title = it.title, video = it.video, voteAverage = it.voteAverage, voteCount = it.voteCount,
                    pageMovie = page
                )
                arrayMovies.add(result)
            }
            return arrayMovies
        }
    }
}