package com.juandavid.openpay.domain.model

import com.juandavid.openpay.data.database.entities.MoviesNowPlayingEntity
import com.juandavid.openpay.data.database.entities.MoviesPopularEntity
import com.juandavid.openpay.data.database.entities.MoviesTopRateEntity
import com.juandavid.openpay.data.model.Results
import com.juandavid.openpay.utils.Constants.converStringtoArray


data class Movie(
    val adult:Boolean,
    val backdrop_path:String,
    val genre_ids: String,
    val id:Long,
    val original_language:String,
    val original_title:String,
    val overview:String,
    val popularity:Double,
    val poster_path:String,
    val release_date:String,
    val title:String,
    val video:Boolean,
    val vote_average:Double,
    val vote_count:Long,
    var page_movie:Long)

fun Results.toDomain() =
    Movie(
        adult = adult!!, backdrop_path = backdropPath!!, genre_ids = genreIds.toString() , id = id!!,
        original_language = originalLanguage!!, original_title =originalTitle!!, overview =overview!!,
        popularity = popularity!!, poster_path = posterPath!!, release_date = releaseDate!!,
        title =title!!, video = video!!,
        vote_average = voteAverage!!, vote_count = voteCount!!, page_movie =pageMovie!!)

fun MoviesPopularEntity.toDomain() =
    Movie(
        adult = adult!!, backdrop_path = backdropPath!!, genre_ids = genreIds.toString(), id = id!!,
        original_language = originalLanguage!!, original_title = originalTitle!!, overview = overview!!,
        popularity = popularity!!, poster_path = posterPath!!, release_date = releaseDate!!, title = title!!, video = video!!,
        vote_average = voteAverage!!, vote_count = voteCount!!, page_movie = pageMovie!!)

fun MoviesNowPlayingEntity.toDomain() =
    Movie(
        adult = adult!!, backdrop_path = backdropPath!!, genre_ids = genreIds.toString(), id = id!!,
        original_language = originalLanguage!!, original_title = originalTitle!!, overview = overview!!,
        popularity = popularity!!, poster_path = posterPath!!, release_date = releaseDate!!, title = title!!, video = video!!,
        vote_average = voteAverage!!, vote_count = voteCount!!, page_movie = pageMovie!!)

fun MoviesTopRateEntity.toDomain() =
    Movie(
        adult = adult!!, backdrop_path = backdropPath!!, genre_ids = genreIds.toString(), id = id!!,
        original_language = originalLanguage!!, original_title = originalTitle!!, overview = overview!!,
        popularity = popularity!!, poster_path = posterPath!!, release_date = releaseDate!!, title = title!!, video = video!!,
        vote_average = voteAverage!!, vote_count = voteCount!!, page_movie = pageMovie!!)