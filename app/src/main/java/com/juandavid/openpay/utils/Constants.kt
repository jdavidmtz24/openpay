package com.juandavid.openpay.utils

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Patterns
import java.util.regex.Pattern


object Constants {

    val SAVE_DATA:String="SAVE_DATA"
    val SAVE_NAME:String="SAVE_NAME"
    val SAVE_EMAIL:String="SAVE_EMAIL"
    val SAVE_PHONE:String="SAVE_PHONE"

    fun converStringtoArray(cadena :String ): ArrayList<Int> = ArrayList<Int>().apply {
        var array= cadena.removeSurrounding("[","]").replace(" ","").split(",").map { it.toInt() }
    }

    fun getDataColumn(
        context: Context, uri: Uri, selection: String?,
        selectionArgs: Array<String?>?,
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.getContentResolver().query(
                uri, projection, selection, selectionArgs,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }
    fun getMedia(context: Context, uri: Uri):String{
        val docId = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()
        val type = split[0]

        var contentUri: Uri? = null
        if ("image" == type) {
            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        } else if ("video" == type) {
            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        } else if ("audio" == type) {
            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        }

        val selection = "_id=?"
        val selectionArgs = arrayOf<String?>(
            split[1]
        )

        return getDataColumn(context, contentUri!!, selection, selectionArgs)!!
    }

    fun getMediaType(context: Context, uri: Uri):Int{
        val docId = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }
            .toTypedArray()
        val type = split[0]

        var contentUri: Uri? = null
        when (type) {
            "image" -> {
                return 1
            }
            "video" -> {
                return 2
            }
            "audio" -> {
                return 3
            }
        }
        return 0
    }

     fun isValidEmail(email: String): Boolean {
        val pattern: Pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }
     fun isValidPhone(email: String): Boolean {
        val pattern: Pattern = Patterns.PHONE
        return pattern.matcher(email).matches()
    }
    fun isLetters(string: String): Boolean {
        for (c in string)
        {
            if (c !in 'A'..'Z' && c !in 'a'..'z') {
                return false
            }
        }
        return true
    }
}