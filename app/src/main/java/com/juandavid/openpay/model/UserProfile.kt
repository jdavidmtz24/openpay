package com.juandavid.openpay.model

data class UserProfile (
var saveData:Boolean = false,
var nameUser:String? = null,
var emailUser:String? = null,
var phoneUser:String? = null
)