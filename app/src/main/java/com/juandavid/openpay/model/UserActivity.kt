package com.juandavid.openpay.model

data class UserActivity (
    var nameUser:String,
    var activityUser:String,
    var photo:String
)
