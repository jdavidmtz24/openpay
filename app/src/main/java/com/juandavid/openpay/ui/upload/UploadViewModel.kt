package com.juandavid.openpay.ui.upload

import android.app.ProgressDialog
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.domain.SendActivityUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class UploadViewModel @Inject constructor(private val sendActivityUserUseCase: SendActivityUserUseCase
) : ViewModel() {

    private val storage = Firebase.storage("gs://openpay-7033a.appspot.com")
    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _messageError = MutableLiveData<String>()
    val messageError: LiveData<String> = _messageError
    private val _userName = MutableLiveData<String>()
    private val _urlUpload= MutableLiveData<String>()
    val urlUpload: LiveData<String> = _urlUpload
    fun uploadFile(file :File,typeFile:Int){
            _isLoading.value = true
            val storageRef= storage.reference
            var openPayRef: StorageReference? = null
            var fileUri = Uri.fromFile(file)
            when (typeFile) {
                1 -> {
                    openPayRef = storageRef.child("image/${fileUri.lastPathSegment}")
                }
                2 -> {
                    openPayRef = storageRef.child("video/${fileUri.lastPathSegment}")
                }
                3 -> {
                    openPayRef = storageRef.child("audio/${fileUri.lastPathSegment}")
                }
            }
            var uploadTask = openPayRef?.putFile(fileUri)
            uploadTask?.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                onInsertData("subiendo Archivo")
                openPayRef?.downloadUrl ?: null
            }?.addOnCompleteListener { task ->
                _isLoading.value = false
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    Log.e("uploadFile", downloadUri.toString())
                    onInsertData("Liga de visualizacion ${downloadUri.toString()}")
                    _urlUpload.value= downloadUri.toString()
                } else {
                   _messageError.value = "No se completo el Guardado del archivo"
                    onInsertData("No se completo el Guardado del archivo")
                }

            }?.addOnFailureListener {
                _isLoading.value = false
                _messageError.value = it.localizedMessage
            }

        }
    fun userName(userName:String){
        _userName.value = userName
    }
    fun onInsertData( activity: String) {
        viewModelScope.launch {
            val listActivities = listOf<ActivitiesUserEntity>(ActivitiesUserEntity(user = _userName.value!!, activity = activity))
            sendActivityUserUseCase.invoke(listActivities)
        }
    }
}