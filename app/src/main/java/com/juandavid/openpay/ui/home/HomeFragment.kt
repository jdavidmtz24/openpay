package com.juandavid.openpay.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.juandavid.openpay.adapter.UserActivityAdapterRecycler
import com.juandavid.openpay.databinding.FragmentHomeBinding
import com.juandavid.openpay.ui.HomeActivity
import com.juandavid.openpay.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var activityHome: HomeActivity
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {


        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        activityHome = activity as HomeActivity
        viewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        /*val textView: TextView = binding.textHome
        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }*/
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        profileUser()
        onObserver()
        binding.btnSend.setOnClickListener {
            val name:String = binding.txtName.text.toString()
            val email:String  = binding.txtEmail.text.toString()
            val phone:String   = binding.txtMob.text.toString()
            if (name.isNullOrBlank()){
                binding.txtInputName.error = "Contenedor vacio"
                return@setOnClickListener
            }
            if (email.isNullOrBlank()){
                binding.txtInputEmail.error = "Contenedor vacio"
                return@setOnClickListener
            }
            if (phone.isNullOrBlank()){
                binding.txtInputMob.error = "Contenedor vacio"
                return@setOnClickListener
            }

            val nameTemp = name.replace(" ","")

            val validateName = Constants.isLetters(nameTemp)
            val validateEmail = Constants.isValidEmail(email)
            val validatePhone = Constants.isValidPhone(phone)

             if (!validateName || !validateEmail || !validatePhone){
                 if (name.length<= 4){
                     binding.txtInputName.error = "Contiene menos de 4 caracteres"
                 }else{
                     if (!validateName){
                         binding.txtInputName.error = "Campo Invalido"
                     }
                 }
                 if (!validateEmail){
                     binding.txtInputEmail.error = "Campo Invalido"
                 }
                 if (!validateEmail){
                     binding.txtInputMob.error = "Campo Invalido"
                 }
             }else{

                 activityHome.saveDataUser(true,name,email,phone)
                 viewModel.onInsertData(name,"Registro de Datos Personales")
                 activityHome.navEnable(true)
                 binding.scrollFeed.visibility = View.GONE
                 binding.constraintDataActivities.visibility = View.VISIBLE
             }

        }
    }

    private fun onObserver() {
        viewModel.activities.observe(viewLifecycleOwner){
            val adapter = UserActivityAdapterRecycler(it)
            loadRecycler(adapter)
        }
        viewModel.textError.observe(viewLifecycleOwner){
            Toast.makeText(requireContext(),it,Toast.LENGTH_LONG).show()
        }
    }

    private fun loadRecycler(adapter: UserActivityAdapterRecycler) {
        binding.recyclerActivities.adapter = adapter
        binding.recyclerActivities.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerActivities.setHasFixedSize(true)
        adapter.notifyDataSetChanged()
    }

    private fun profileUser() {
        lifecycleScope.launch {
           activityHome.getSaveData().collect{
               activityHome.navEnable(it.saveData)
               if (it.saveData){
                   binding.scrollFeed.visibility = View.GONE
                   binding.constraintDataActivities.visibility = View.VISIBLE
                   binding.textWelcome.text = "Bienvenido(a)\n${it.nameUser}"
                   viewModel.onCreate(it.nameUser!!)
               }else{
                   binding.constraintDataActivities.visibility = View.GONE
                   binding.scrollFeed.visibility = View.VISIBLE
               }
           }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}