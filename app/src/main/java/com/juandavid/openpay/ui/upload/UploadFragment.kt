package com.juandavid.openpay.ui.upload

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toFile
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.juandavid.openpay.R
import com.juandavid.openpay.databinding.FragmentUploadBinding
import com.juandavid.openpay.ui.HomeActivity
import com.juandavid.openpay.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import java.io.File


@AndroidEntryPoint
class UploadFragment : Fragment() {


    private var _binding: FragmentUploadBinding? = null
    private val binding get() = _binding!!
    private lateinit var activityHome:HomeActivity
    private lateinit var viewModel: UploadViewModel
    private val FILE_SELECT_CODE= 1000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHome = activity as HomeActivity
        viewModel = ViewModelProvider(this)[UploadViewModel::class.java]
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = FragmentUploadBinding.inflate(inflater, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonNavExplorer.setOnClickListener {
            if (activityHome.isOnline()) {
                val intent = Intent()
                    .setType("*/*")
                    .setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, FILE_SELECT_CODE)
                viewModel.onInsertData("Abrir Explorador de Archivos")
            }else{
                alertError("No cuentas con conexion a Internet para enviar el archivo al repositorio")
            }
        }
        viewModel.userName(activityHome.userName)
        observesViewModel()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode ==  FILE_SELECT_CODE) {
            if (resultCode == RESULT_OK){
                if (activityHome.isOnline()) {
                    val filePath = data?.data?.path
                    if (filePath != null) {
                        val uri: Uri?=data?.data
                        var file:File? = null
                        try {
                            if (uri != null) {
                                file = File(Constants.getMedia(requireContext(),uri))
                                Log.e("URI Type",
                                    Constants.getMediaType(requireContext(),uri).toString()
                                )
                            }
                        } catch (e: Exception) {
                            Log.e("URI",e.localizedMessage)
                        }

                        if (file != null) {
                            if (file.exists()){
                                Log.e("Existe",file.absolutePath)
                                viewModel.uploadFile(file,Constants.getMediaType(requireContext(),uri!!))
                            }else{
                                Log.e("No existe",file.absolutePath)
                            }
                        }
                    }
                }
                else{
                    alertError("No cuentas con conexion a Internet para enviar el archivo al repositorio")
                }
            }else if (resultCode == RESULT_CANCELED){
                Log.e("RESULT_CANCELED",data.toString())
                alertError(title = "Explorador de Archivos", message = "Proceso cancelado por el Usuario")
            }
        }
    }
    private fun observesViewModel() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setTitle("Subiendo Archivo")
        progressDialog.setMessage("Por favor espera")
        viewModel.isLoading.observe(viewLifecycleOwner){
            if (it) progressDialog.show() else progressDialog.dismiss()
        }
        viewModel.messageError.observe(viewLifecycleOwner){mensaje->
            alertError(mensaje)
        }
        viewModel.urlUpload.observe(viewLifecycleOwner){
            alertURL(it)
        }
    }
    private fun alertURL(url:String){
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Archivo Almacenado")
            .setMessage("¿Desea visualizar el archivo almacenado?")
            .setPositiveButton("Aceptar") { view, _ ->
                val url = url
                val intent =Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse(url))
                startActivity(intent)
                view.dismiss()
            }
            .setNegativeButton("Cancelar"){view, _ ->
                view.dismiss()
            }
            .setCancelable(false)
            .create()
        dialog.show()
    }
    private fun alertError(message:String){
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle("Error Subiendo Archivo")
            .setMessage(message)
            .setPositiveButton("Aceptar") { view, _ ->
                view.dismiss()
            }
            .setCancelable(false)
            .create()
        dialog.show()
    }
    private fun alertError(title:String,message:String){
        val dialog = AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Aceptar") { view, _ ->
                view.dismiss()
            }
            .setCancelable(false)
            .create()
        dialog.show()
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}