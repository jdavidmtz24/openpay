package com.juandavid.openpay.ui.list_map

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.juandavid.openpay.R
import com.juandavid.openpay.databinding.FragmentListLocationBinding
import com.juandavid.openpay.ui.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListMapFragment : Fragment(), OnMapReadyCallback {

    private var _binding: FragmentListLocationBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var map:GoogleMap
    private lateinit var activityHome:HomeActivity
   private lateinit var listMapViewModel: ListMapViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        activityHome = activity as HomeActivity
        listMapViewModel =
            ViewModelProvider(this).get(ListMapViewModel::class.java)

        _binding = FragmentListLocationBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val supportMapFragment = childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment
        supportMapFragment.getMapAsync(this)
        listMapViewModel.onInsertData(userName = activityHome.userName, activity = "Visualizar Marcadores de Ubicacion de Movil")
    }

    private fun createMapFragment() {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(p0: GoogleMap) {
        map = p0
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        map.isMyLocationEnabled = true
        var locationDemo = LatLng(0.0,0.0)
        val activityHome = activity as HomeActivity
        val arrayListMap = activityHome.arrayList
        if (arrayListMap.isNotEmpty()) {
            arrayListMap.forEachIndexed { index, location ->
                Log.e("arrayListMap","$index ${location.toString()}")
                locationDemo =LatLng(location.latitud.toDouble(),location.longitud.toDouble())
                map.addMarker(
                    MarkerOptions()
                        .position(locationDemo)
                        .title("${location.marca}")
                )
            }
        }
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(locationDemo, 14.0F))
    }
}