package com.juandavid.openpay.ui.movie

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.juandavid.openpay.R
import com.juandavid.openpay.adapter.MovieAdapterRecycler
import com.juandavid.openpay.databinding.FragmentMovieBinding
import com.juandavid.openpay.ui.HomeActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesFragment : Fragment() {

    private var _binding: FragmentMovieBinding? = null
    private lateinit var activityHome: HomeActivity
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var moviesViewModel: MoviesViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityHome = activity as HomeActivity
        moviesViewModel = ViewModelProvider(this)[MoviesViewModel::class.java]
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val typesMovies =  resources.getStringArray(R.array.MoviesType)
        val adapterSpinner= ArrayAdapter<String> (requireContext(), android.R.layout.simple_spinner_dropdown_item,typesMovies)
        //adapterSpinner.setDropDownViewResource(androidx.transition.R.layout.support_simple_spinner_dropdown_item)
        binding.spinnerTypeMovie.adapter = adapterSpinner
        binding.spinnerTypeMovie.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                moviesViewModel.onLoadMovies(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
              //  TODO("Not yet implemented")
            }

        }
        if (activityHome.isOnline()) {
            moviesViewModel.onCreate(activityHome.userName)
        }else{
            alertConnection()
        }
        observesViewModel()
    }

    override fun onResume() {
        super.onResume()
        if (activityHome.isOnline()) {
            moviesViewModel.onCreate(activityHome.userName)
        }else{
            alertConnection()
        }
    }
    private fun alertConnection(){
        val positiveButtonClick = {dialog: DialogInterface, which:Int->
            moviesViewModel.onCreate(activityHome.userName)
            dialog.dismiss()
        }
        val negativeButtonClick = {dialog: DialogInterface, which:Int->
            Toast.makeText(requireContext(),"No se puede mostrar el contenido",Toast.LENGTH_SHORT).show()
            dialog.dismiss()
        }
        val builder = AlertDialog.Builder(context)
        with(builder){
            setTitle("Conexion Movil")
            setMessage("No cuentas con conexion a Internet")
            setPositiveButton("Reintentar ",DialogInterface.OnClickListener(function =positiveButtonClick))
            setNegativeButton("Cancelar", DialogInterface.OnClickListener(function = negativeButtonClick))
        }
    }
    private fun observesViewModel() {
        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setTitle("Cargando")
        progressDialog.setMessage("Por favor espera")
        moviesViewModel.adapter.observe(viewLifecycleOwner){
            if (!it.isNullOrEmpty()){
                val adapter = MovieAdapterRecycler(it)
                binding.recyclerMovies.adapter = adapter
                binding.recyclerMovies.layoutManager = LinearLayoutManager(context)
                binding.recyclerMovies.setHasFixedSize(true)
                adapter.notifyDataSetChanged()
            }
        }
        moviesViewModel.isLoading.observe(viewLifecycleOwner){
            if (it) progressDialog.show() else progressDialog.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}