package com.juandavid.openpay.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.domain.GetActivitiesUserUseCase
import com.juandavid.openpay.domain.SendActivityUserUseCase
import com.juandavid.openpay.model.UserActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getActivitiesUserUseCase: GetActivitiesUserUseCase,
    private val sendActivityUserUseCase: SendActivityUserUseCase
) : ViewModel() {

    private val _textError = MutableLiveData<String>()
    val textError: LiveData<String> = _textError

    private val _activities = MutableLiveData<ArrayList<UserActivity>>()
    val activities: LiveData<ArrayList<UserActivity>> = _activities
    fun onCreate(user:String){
        viewModelScope.launch {
            val response = getActivitiesUserUseCase.invoke(user)
            if (!response.isNullOrEmpty()){
               var arrayActivities: ArrayList<UserActivity> = arrayListOf()
                response.forEach{
                    arrayActivities.add(UserActivity(nameUser = it.user!!, activityUser = it.activity!!, photo = ""))
                }
                _activities.value = arrayActivities
            }else{
                _textError.value = "No tienes actividades"
            }

        }
    }

    fun onInsertData(name: String, activity: String) {
        viewModelScope.launch {
            val listActivities = listOf<ActivitiesUserEntity>(ActivitiesUserEntity(user = name, activity = activity))
            sendActivityUserUseCase.invoke(listActivities)
        }
    }
}