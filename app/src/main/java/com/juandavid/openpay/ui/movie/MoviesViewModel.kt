package com.juandavid.openpay.ui.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.openpay.adapter.MovieAdapterRecycler
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.data.model.Results
import com.juandavid.openpay.domain.GetMovieNowPlayingUseCase
import com.juandavid.openpay.domain.GetMoviePopularUseCase
import com.juandavid.openpay.domain.GetMovieTopRatedUsecase
import com.juandavid.openpay.domain.SendActivityUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val getMoviePopularUseCase: GetMoviePopularUseCase,
    private val getMovieNowPlayingUseCase: GetMovieNowPlayingUseCase,
    private val getMovieTopRatedUsecase: GetMovieTopRatedUsecase,
    private val sendActivityUserUseCase: SendActivityUserUseCase
) : ViewModel() {

    private val _textError = MutableLiveData<String>()
    val textError: LiveData<String> = _textError

    private val _adapter = MutableLiveData<ArrayList<Results>>()
    val adapter: LiveData<ArrayList<Results>> = _adapter

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _userName = MutableLiveData<String>("")
    fun onCreate(userName:String) {
        _userName.value = userName
        viewModelScope.launch {
            _isLoading.value = true
            sendActivityUserUseCase.invoke(listOf(ActivitiesUserEntity(user =_userName.value!!,
                activity = "Ver Lista de Peliculas Populares")))
            getMoviePopularUseCase.invoke("en-US",1)
            val result = getMoviePopularUseCase.invoke("en-US",1)
            if (result.isNullOrEmpty()){
                _textError.value="No se encontraron Resultados"
            }else{
                _adapter.value = result!!
            }
            _isLoading.value = false
        }
    }
    fun onLoadMovies(typeMovie:Int) {
        viewModelScope.launch {
            _isLoading.value = true
            val result = when (typeMovie) {
                0 -> {
                    sendActivityUserUseCase.invoke(listOf(ActivitiesUserEntity(user =_userName.value!!,
                        activity = "Ver Lista de Peliculas Populares")))
                    getMoviePopularUseCase.invoke("en-US",1)

                }
                1 -> {
                    sendActivityUserUseCase.invoke(listOf(ActivitiesUserEntity(user =_userName.value!!, activity =  "Ver Lista de Peliculas Recientes")))
                    getMovieNowPlayingUseCase.invoke("en-US",1)
                }
                2 -> {
                    sendActivityUserUseCase.invoke(listOf(ActivitiesUserEntity(user =_userName.value!!, activity =  "Ver Lista de Peliculas Mejor Calificadas")))
                    getMovieTopRatedUsecase.invoke("en-US",1)
                }
                else -> {
                    sendActivityUserUseCase.invoke(listOf(ActivitiesUserEntity(user =_userName.value!!, activity =  "Ver Lista de Peliculas Mejor Calificadas")))
                    getMoviePopularUseCase.invoke("en-US",1)

                }
            }
            if (result.isNullOrEmpty()){
                _textError.value="No se encontraron Resultados"
            }else{
                _adapter.value = result!!
            }
            _isLoading.value = false
        }
    }
}