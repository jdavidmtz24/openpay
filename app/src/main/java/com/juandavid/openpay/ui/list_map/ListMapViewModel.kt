package com.juandavid.openpay.ui.list_map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.juandavid.openpay.data.database.entities.ActivitiesUserEntity
import com.juandavid.openpay.domain.SendActivityUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListMapViewModel @Inject constructor(
   private val sendActivityUserUseCase: SendActivityUserUseCase
): ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    fun onInsertData( userName:String,activity: String) {
        viewModelScope.launch {
            val listActivities = listOf<ActivitiesUserEntity>(ActivitiesUserEntity(user = userName, activity = activity))
            sendActivityUserUseCase.invoke(listActivities)
        }
    }
}