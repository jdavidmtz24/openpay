package com.juandavid.openpay.ui

import android.Manifest
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.view.animation.AnticipateInterpolator
import android.widget.RemoteViews
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.juandavid.openpay.R
import com.juandavid.openpay.databinding.ActivityHomeBinding
import com.juandavid.openpay.model.UserProfile
import com.juandavid.openpay.utils.Constants
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Locale
import java.util.Objects


val Context.dataStore by preferencesDataStore(name = "USER_PREFERENCE")
@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionId = 2
    private val timeSend =1000 * 60 * 5
    private lateinit var database: DatabaseReference
    lateinit var navView: BottomNavigationView
    lateinit var userName:String
// ...

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navView = binding.navView
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            splashScreen.setOnExitAnimationListener { splashScreenIt->
                    val slideUp = ObjectAnimator.ofFloat(
                        splashScreenIt,
                        View.TRANSLATION_Y,
                        0f,
                        -splashScreenIt.height.toFloat()
                    )
                    slideUp.interpolator = AnticipateInterpolator()
                    slideUp.duration = 200L

                slideUp.doOnEnd {
                    splashScreenIt.remove()
                }
                slideUp.start()
            }
        }
        val navController = findNavController(R.id.nav_host_fragment_activity_home)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_user,
                R.id.navigation_movies,
                R.id.navigation_location,
                R.id.navigation_upload
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        getFirebase()
        val minutes:Long = timeSend.toLong()
        val handler: Handler = Handler()
        val run = object : Runnable {
            override fun run() {
                if (isOnline()) {
                   onSendLocation()
                }
                handler.postDelayed(this, minutes)// 5 minutes
            }
        }
        handler.post(run)

    }

    private fun onSendLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        getLocation()
    }
    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
            &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_PHONE_STATE
            ) == PackageManager.PERMISSION_GRANTED
            &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
            // Manifest.permission.READ_EXTERNAL_STORAGE
        ) {
            return true
        }
        return false
    }
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            permissionId
        )
    }
    @SuppressLint("MissingSuperCall")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        if (requestCode == permissionId) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLocation()
            }
        }
    }
    var deviceId: String = ""

    @SuppressLint("MissingPermission", "SetTextI18n")
    private fun getLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location != null) {
                        val geocoder = Geocoder(this, Locale.getDefault())
                        val list: MutableList<Address>? =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        Log.e("getLocation()",list.toString())
                        val marca = Build.MANUFACTURER.toUpperCase()
                        val modelo = Build.MODEL.toUpperCase()
                        val fecha = getDate()
                        val hora = getHour()
                        val latitud = location.latitude
                        val longitud = location.longitude


                        deviceId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            Settings.Secure.getString(
                                this.getContentResolver(),
                                Settings.Secure.ANDROID_ID
                            )
                        } else {
                            val mTelephony =
                                this.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                            if (mTelephony.deviceId != null) {
                                mTelephony.deviceId
                            } else {
                                Settings.Secure.getString(
                                    this.getContentResolver(),
                                    Settings.Secure.ANDROID_ID
                                )
                            }
                        }
                        Log.e("IMEI",deviceId.toString())
                        val locationFirebase = com.juandavid.openpay.data.firebase.model.Location(
                            fecha = fecha,
                            hora = hora,
                            latitud = latitud.toString(),
                            longitud = longitud.toString(),
                            marca = marca,
                            modelo = modelo,
                            imei = deviceId
                        )
                        Log.e("getLocation()",locationFirebase.toString())
                        //addNotification()

                        writeLocationFirebase(locationFirebase)
                    }
                }
            } else {
                Toast.makeText(this, "Please turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun getDate():String{
        val c = Calendar.getInstance()

        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val mesTemp = if (month<10) "0$month" else month
        val diaTemp = if (day<10) "0$day" else day
        return "$diaTemp/$mesTemp/$year"
    }
    private fun getHour():String{
        val c = Calendar.getInstance()

        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        val second = c.get(Calendar.SECOND)
        val horaTemp = if (hour<10) "0$hour" else hour
        val minutoTemp = if (minute<10) "0$minute" else minute
        val secondTemp =   if (second<10) "0$second" else second
        return "$horaTemp:$minutoTemp:$secondTemp"
    }
    private fun getFirebase() {
        database = Firebase.database.reference
    }
    private fun writeLocationFirebase(locationData: com.juandavid.openpay.data.firebase.model.Location) {
        val mHashmap: MutableMap<String, Any> = HashMap()

        mHashmap["imei"] = locationData.imei
        mHashmap["fecha"] = locationData.fecha
        mHashmap["hora"] = locationData.hora
        mHashmap["latitud"] = locationData.latitud
        mHashmap["longitud"] = locationData.longitud
        mHashmap["marca"] = locationData.marca
        mHashmap["modelo"] = locationData.modelo
        database.child("location")
            .child("${getDate().replace("/", "")}${getHour().replace(":", "")}")
            .setValue(mHashmap)
        addNotificationDos()
        readLocationFirebase()
        //addNotificationDos()
    }
    var arrayList:ArrayList<com.juandavid.openpay.data.firebase.model.Location> = arrayListOf()
    fun readLocationFirebase(){
        database.child("location")
            .get().addOnSuccessListener {
                val data = it
                for(ds in data.children) {
                    val imeiMobile = ds.hasChild("imei")
                    if (imeiMobile) {
                        val imei = ds.child("imei")
                        if (imei.value.toString().equals(deviceId)) {
                            val fecha = ds.child("fecha")
                            val hora = ds.child("hora")
                            val latitud = ds.child("latitud")
                            val longitud = ds.child("longitud")
                            val marca = ds.child("marca")
                            val modelo = ds.child("modelo")
                            val location =
                                com.juandavid.openpay.data.firebase.model.Location(
                                    fecha = fecha.value as String,
                                    hora = hora.value as String,
                                    latitud =  latitud.value as String,
                                    longitud = longitud.value as String,
                                    imei = imei.value as String,
                                    marca =  marca.value as String,
                                    modelo = modelo.value as String
                                )
                            arrayList.add(location)
                        }
                    }
                }
            }.addOnFailureListener {
                Log.e("firebase", "Error getting data", it)
            }
    }
    private fun addNotification() {
        val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this,"OpenPay")
            .setSmallIcon(R.drawable.send)
            .setContentTitle("Almacenamiento de Ubicacion")
            .setContentText("Se almaceno la ubicacion del dispositivo")
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setStyle(NotificationCompat.BigTextStyle().bigText("Se almaceno la ubicacion del dispositivo"))
        // Add as notification
        manager.notify(0, builder.build())
    }
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "i.apps.notifications"
    private val description = "Test notification"
    private fun addNotificationDos(){
        val contentView = RemoteViews(packageName, R.layout.layout_after_notification)

        // checking if android version is greater than oreo(API 26) or not
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, channelId)
                .setContent(contentView)
                .setSmallIcon(R.drawable.send)
                .setContentTitle("Almacenamiento de Ubicacion")
                .setContentText("Se almaceno la ubicacion del dispositivo")
                .setColor(com.google.android.material.R.attr.colorOnPrimary)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_launcher_background))
        } else {

            builder = Notification.Builder(this)
                .setContent(contentView)
                .setSmallIcon(R.drawable.send)
                .setContentTitle("Almacenamiento de Ubicacion")
                .setContentText("Se almaceno la ubicacion del dispositivo")
                .setColor(com.google.android.material.R.attr.colorOnPrimary)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_launcher_background))
        }
        notificationManager.notify(1234, builder.build())
    }


    fun isOnline(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                } else {
                    @Suppress("DEPRECATION")
                    val networkInfo =
                        connectivityManager.activeNetworkInfo ?: return false
                    @Suppress("DEPRECATION")
                    return networkInfo.isConnected
                }
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
    fun saveDataUser(savePreference:Boolean, userName:String, email:String, phone:String){
        lifecycleScope.launch (Dispatchers.IO){
            dataStore.edit {preferences->
                preferences[booleanPreferencesKey(Constants.SAVE_DATA)] = savePreference
                preferences[stringPreferencesKey(Constants.SAVE_NAME)] = userName
                preferences[stringPreferencesKey(Constants.SAVE_EMAIL)] = email
                preferences[stringPreferencesKey(Constants.SAVE_PHONE)] = phone
            }
        }
    }
    fun getSaveData() = dataStore.data.map {preferences->
        userName = preferences[stringPreferencesKey(Constants.SAVE_NAME)].orEmpty()
        UserProfile(
            saveData = preferences[booleanPreferencesKey(Constants.SAVE_DATA)] ?: false,
            nameUser = preferences[stringPreferencesKey(Constants.SAVE_NAME)].orEmpty(),
            emailUser =  preferences[stringPreferencesKey(Constants.SAVE_EMAIL)].orEmpty(),
            phoneUser = preferences[stringPreferencesKey(Constants.SAVE_PHONE)].orEmpty()
        )

    }
    fun navEnable(enable:Boolean){
        navView.visibility = if (enable) View.VISIBLE else View.GONE
    }
}