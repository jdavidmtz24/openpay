package com.juandavid.openpay.di

import android.content.Context
import androidx.room.Room
import com.juandavid.openpay.data.database.MovieDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    private const val MOVIE_DATA_BASE="MOVIE_DATABASE"

    @Singleton
    @Provides
    fun providesRoom(@ApplicationContext context:Context)
    = Room.databaseBuilder(context, MovieDatabase::class.java, MOVIE_DATA_BASE)
        .allowMainThreadQueries()
        .build()

    @Singleton
    @Provides
    fun providesMovieDao(dao:MovieDatabase) =  dao.getMovieDao()
}