package com.juandavid.openpay.adapter

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.openpay.R
import com.juandavid.openpay.model.UserActivity

class UserActivityAdapterRecycler(arrayActivities:ArrayList<UserActivity>): RecyclerView.Adapter<UserActivityAdapterRecycler.ViewHolder>() {
    private var arrayUserActivities:ArrayList<UserActivity> = arrayListOf()

    init {
        arrayUserActivities = arrayActivities
    }
    class ViewHolder(item: View):RecyclerView.ViewHolder(item){
        val itemNameUser: TextView
        val itemActivityUser: TextView
        init {
            itemNameUser = item.findViewById(R.id.text_user)
            itemActivityUser = item.findViewById(R.id.text_activity)
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ):ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_activity_user, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserActivityAdapterRecycler.ViewHolder, position: Int) {
        val modelUserActivity = arrayUserActivities[position]
        holder.itemNameUser.text = modelUserActivity.nameUser
        holder.itemActivityUser.text = modelUserActivity.activityUser
    }

    override fun getItemCount(): Int {
       return arrayUserActivities.size
    }
}