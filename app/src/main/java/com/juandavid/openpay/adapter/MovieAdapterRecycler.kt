package com.juandavid.openpay.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.juandavid.openpay.BuildConfig
import com.juandavid.openpay.R
import com.juandavid.openpay.data.model.Results
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class MovieAdapterRecycler(arrayMovies:ArrayList<Results>): RecyclerView.Adapter<MovieAdapterRecycler.ViewHolder>() {
    private val arrayMoviesModel:ArrayList<Results>
    init {
        arrayMoviesModel = arrayMovies
    }

    class ViewHolder(item: View):RecyclerView.ViewHolder(item){
        val itemImageMovie: ImageView
        val itemTitleMovie: TextView
        val itemDetailMovie: TextView
        val itemDateMovie: TextView
        val itemProgressBar: ProgressBar
        init{
            itemImageMovie = item.findViewById(R.id.imageview_movie_item)
            itemTitleMovie = item.findViewById(R.id.textview_title_movie)
            itemDateMovie = item.findViewById(R.id.textview_producer_movie)
            itemDetailMovie= item.findViewById(R.id.textview_date_movie)
            itemProgressBar = item.findViewById(R.id.progress_item)
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie,parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieAdapterRecycler.ViewHolder, position: Int) {
        holder.itemProgressBar.isVisible = true
        val modelMovies: Results =  arrayMoviesModel[position]
        try {
            val urlImageMovie = if (!modelMovies.posterPath.isNullOrBlank()) BuildConfig.BASE_IMAGE_URL+modelMovies.posterPath else BuildConfig.BASE_IMAGE_URL+modelMovies.backdropPath
            Picasso.get().load(urlImageMovie).into(holder.itemImageMovie, object : Callback {
                override fun onSuccess() {
                    holder.itemProgressBar.isVisible =  false
                }

                override fun onError(e: Exception?) {
                    holder.itemProgressBar.isVisible =  false
                }
            })
            holder.itemTitleMovie.text =  modelMovies.title
            holder.itemDateMovie.text = modelMovies.releaseDate
            holder.itemDetailMovie.text= modelMovies.overview
            holder.itemView.setOnClickListener {
              //  onClickListener.onClick(modelMovies)
            }
        } catch (nullpointer:java.lang.NullPointerException) {
            holder.itemProgressBar.isVisible = false
        }
    }

    override fun getItemCount(): Int {
      return  arrayMoviesModel.size
    }
}