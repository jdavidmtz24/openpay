# OpenPay


# PruebaTecnicaIronbit	

## Comandos de Clonacion de Repositorio


```
cd existing_repo
git remote add origin https://gitlab.com/jdavidmtz24/openpay.git
git branch -M main
git push -uf origin main
```

## Implementaciones en Proyecto

En el proyecto se muestran los puntos solicitados por el cliente, en el se utilizo lo siguiente

- Lenguaje de Programacion Kotlin - Se utiliza el lenguaje de Programacion Kotlin en todo el proyecto 

- Databinding - Se utiliza el Databinding para la vinculacion de componentes IU mediante formato declarativo

- ThemeDark - Se crea estilo de colores para modo Oscuro y Claro
 
- Patron de diseño MVVM - donde se agregan peticiones en red consumiendo un API [TMDB API.](https://api.themoviedb.org) con Retrofit 2 y corrutinas. También se añadido clean architecture.

- Dagger Hilt - Inyección de dependencias - Se ha añadido inyección de dependencias en todo el proyecto con Dagger Hilt 

- Almacenamiento de Datos con la Libreria Room para la persistencia de Datos ademas de DataStorePreferences

- Orientacion de Pantalla - Se agregan plantillas para la rotacion de pantalla en Tabletas en la Vista de Detalle

- Se agrega liga de descarga del apk para prueba (https://drive.google.com/file/d/1May-a9CDQQyPZLWLwY63mjUUTlQjABlZ/view?usp=sharing)


